/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2ox;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Lab2OX {
    static char[][]table = {{'-','-','-'},
        {'-','-','-'},
        {'-','-','-'}};
    static char currentPlayer = 'X';
    static int row, col;
    static void printWelcome(){
        System.out.println("Welcome to OX");
    } 
    static void printTable() {
         for(int i=0;i<3;i++){
             for(int j=0;j<3;j++){
                System.out.print(table[i][j]+ " "); 
             }
             System.out.println(" ");
                
         }
     }
    
    static void printTurn(){
        System.out.println(currentPlayer + " Turn");
    }
    
    static void inputRowcol(){
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input roe col :");
            row = sc.nextInt();
            col = sc.nextInt();
            if  (table[row-1][col-1] == '-'){
                table[row-1][col-1] = currentPlayer;
                break;
            }
        }
    }
    
    
    
    static void switchPlayer(){
        if(currentPlayer=='X'){
            currentPlayer ='O';
        }else {
            currentPlayer ='X';
        }
    }
    static boolean inWin() {
    if (checkRow() || checkCol() || checkDiagonal()) {
        return true;
    }
    return false;
}
    static boolean checkRow(){
        for(int i =0;i<3;i++) {
            if(table[row-1][i]!=currentPlayer) {
                return false;
            }
        }
       return true;
    }
    static boolean checkCol() {
        for(int i =0;i<3;i++) {
            if(table[i][col-1]!=currentPlayer) {
                return false;
            }
        }
       return true;
    }
    static boolean checkDiagonal() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
        return true;
    }
    if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
        return true;
    }
    
    return false;
}
    static boolean isDraw() {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (table[i][j] == '-') {
                return false;
            }
        }
    }
    return true;
}
    static void Continue() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input continue or exit (y/n): ");
        String Continue = sc.next();
        if (Continue.equalsIgnoreCase("n")) {
            System.exit(0);
        } else if (Continue.equalsIgnoreCase("y")) {
            resetGame();
            printWelcome();
        } else {
            System.out.println("Invalid input. Please try again.");
            Continue();
        }
    }

    static void resetGame() {
        table = new char[][] {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}
        };
        currentPlayer = 'X';
    }

static void printWin(){
        System.out.println(currentPlayer + " is a winnerl");
    }
     public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowcol();
            if (inWin()) {
                printTable();
                printWin();
                Continue();
            }
            if (isDraw()) {
                System.out.println("Tie! No one wins!");
                Continue();
            }
            switchPlayer();
        }
    }
}
